export class DestinoViaje {
  public selected: boolean;
  public servicios: string[];
  public Preferencias: number;
  constructor(public nombre: string, public imagenUrl:string) {
    this.servicios = ['picina','desayuno','parking','cena','wifi','labanderia',];
    this.Preferencias=0;
  }
  isSelected(): boolean{
    return this.selected;
  }
  setSelected(s: boolean){
    this.selected = s;
  }
  Aumentanum(){
    this.Preferencias++;
  }
  Reducenum(){
    this.Preferencias--;
  }
  Cuanto():number{
    return this.Preferencias;
  }
}
