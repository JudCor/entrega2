import { DestinoViaje } from './destino-viaje.models';
import { Subject, BehaviorSubject } from 'rxjs';


export class DestinosApiClient {
	destinos:DestinoViaje[];
	Actual: Subject<DestinoViaje>= new BehaviorSubject<DestinoViaje>(null);

	constructor() {
       this.destinos = [];
	}
	add(d:DestinoViaje){
	  this.destinos.push(d);
	}
	getAll(){
	  return this.destinos;
  }

  	// hacemos supuesta conexion con servidor
	elegir(d:DestinoViaje){
		this.destinos.forEach(x => x.setSelected(false));
		this.Actual.next(d);
		d.setSelected(true);
	}

	subscribeOnchange(fn){
		this.Actual.subscribe(fn);
	}

}
