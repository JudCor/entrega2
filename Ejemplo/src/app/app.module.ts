import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoComponent } from './form-destino/form-destino.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';

import { DestinoViaje} from './models/destino-viaje.models';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { DestinosViajesState, reducerDestinosViajes ,intializeDestinosViajesState, DestinosViajesEffects} from './models/destinos-viajes-state.model';
import { StoreModule as NgRxStoreModule, ActionReducerMap}from '@ngrx/store'
import {EffectsModule} from '@ngrx/effects'

const rutas: Routes =[
  {path: '', redirectTo:'home', pathMatch: 'full'},
  {path:'home', component: ListaDestinosComponent},
  {path:'destino', component: DestinoDetalleComponent},
];
// Redux init
export interface AppState {
  destinos: DestinosViajesState;

}

const reducers: ActionReducerMap<AppState>={
  destinos: reducerDestinosViajes
}

let reducersInitialState = {
  destinos: intializeDestinosViajesState()
};
// redux fin init

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    DestinoDetalleComponent,
    FormDestinoComponent,
    ListaDestinosComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot(rutas),
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects])
  ],
  providers: [
    DestinosApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
