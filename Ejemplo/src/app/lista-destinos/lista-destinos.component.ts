import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { ElegidoFavoritoAction } from './../models/destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import {AppState} from '../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
//arraydestinos:DestinoViaje[]; 
@Output() onItemAded: EventEmitter<DestinoViaje>;
titulo:string;
listafavoritos: string[];
  //constructor() {
    //this.arraydestinos = [];
    
    constructor(public destinosApiClient:DestinosApiClient ) {
      this.onItemAded= new EventEmitter();
      this.titulo="Formulario";
      this.listafavoritos=[];
      this.destinosApiClient.subscribeOnchange((d:DestinoViaje) => {
        if(d != null){
          this.listafavoritos.push('se a elegido '+d.nombre);
        }
      });
   }

  ngOnInit(): void {
  }

  agregado(d : DestinoViaje){
    //let x = new DestinoViaje(nombre,url);
    this.destinosApiClient.add(d);
    this.onItemAded.emit(d);
    this.destinosApiClient.elegir(d);
  }

/*
  guardar(nombre:string,url:string):boolean{
    //this.arraydestinos.push(new DestinoViaje(nombre,url));
    //console.log(this.arraydestinos);
    let x = new DestinoViaje(nombre,url);
    this.destinosApiClient.add(x);
    this.onItemAded.emit(x);
    return false;
  } */
  elegido(e:DestinoViaje){
    this.destinosApiClient.elegir(e);
    
    //this.arraydestinos.forEach(x => x.setSelected(false));
    // //cambio 2 para agregar lista de elegidos
    ////this.destinosApiClient.getAll().forEach(d => d.setSelected(false)); 
    ////d.setSelected(true);
    ///this.destinosApiClient.elegir(d);
    ///this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
//DEsde aquy se daña todo 