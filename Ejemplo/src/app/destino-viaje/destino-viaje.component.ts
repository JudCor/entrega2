import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destinos: DestinoViaje;
  @Input() position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  numero:number;

  constructor() {
    this.clicked = new EventEmitter();
    this.numero=5;
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destinos);  //Enviarle al componente padre.
    return false;
  }

  Satisfecho(){
    this.numero++;
    return false;
  }

  NoSatisfecho(){
    this.numero--;
    return false;
  }

}
