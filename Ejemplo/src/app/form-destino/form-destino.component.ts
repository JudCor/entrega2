import { Component, OnInit, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models'
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { fromEvent, from } from 'rxjs';
import { map ,filter , debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import { ajax, AjaxResponse} from 'rxjs/ajax'

@Component({
  selector: 'app-form-destino',
  templateUrl: './form-destino.component.html',
  styleUrls: ['./form-destino.component.css']
})
export class FormDestinoComponent implements OnInit {
  @Output() onItemAded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  Longitudminima=4;
  searchResults: string[];

  constructor(fb: FormBuilder) { 
    this.onItemAded=new EventEmitter();
    this.fg= fb.group({
      nombre:['', Validators.compose([
        Validators.required,
        this.Longitud,
        this.nombreParametrizable(this.Longitudminima)
      ])],
      url:[''],
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio algo :' + form);
    });
  }

  
  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e:KeyboardEvent)=>(e.target as HTMLInputElement).value),
        filter(Text=> Text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(()=> ajax('/assets/datos.json'))
        ).subscribe(AjaxResponse=> {
          this.searchResults = AjaxResponse.response;
        });
      
  }

  guardar(nombre:string,url:string):boolean{
    let d = new DestinoViaje(nombre,url);
    this.onItemAded.emit(d);
    return false;
  }
  // creando validadores 
  Longitud(control:FormControl):{[s:string]:boolean}{
    const l= control.value.toString().trim().length;
    if(l>0 && l<5){
      return{'NombreInvalido':true}
    }
    return null;
  }
  nombreParametrizable(Longitudminima:number): ValidatorFn{
    return(control:FormControl):{ [s: string]:boolean} | null => {
      const l= control.value.toString().trim().length;
      if(l>0 && l<Longitudminima){
        return{validadorLongitud:true};
      }
      return null;
    }
  }    
  
}
